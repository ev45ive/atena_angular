angular.module('users')
.component('userCard', {
	templateUrl: 'users/user.tpl.html',
	bindings:{
		title: '@',
		user: '=',
	},
	transclude:true,
	controller: UserCardCtrl
	//controllerAs:'$ctrl',
})

function UserCardCtrl(){
	this.dane = 1 ; // == {{$ctrl.dane}}
}

/*
	TODO:
	- UserDetail Component
		- onEdit - ????
		- onCancel - UsersCtrl.select(null)

	- UserForm Component
		- onSave - UsersCtrl.saveUser(user)
		- onCancel - UsersCtrl.select(null)

	Pluralsight

*/