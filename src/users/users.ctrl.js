angular.module('users')
.controller('UsersCtrl', UsersCtrl)

function UsersCtrl(Users) {

	Users.getUsers().then(function(querySet){
		this.users = querySet
	}.bind(this))

	this.selected = null;

	this.saveUser = function(user){
		Users.saveUser(user)
	}

	this.select = function(user){
		// event.stopPropagation();
		this.selected = user;
	}
}