angular.module('users')
.directive('notContain', function() {

	return {
		restrict:'A',
		require: 'ngModel',
		link: function(scope, element, attrs, ngModelCtrl){

			// scope.$watch(attrs['notContain'], function(newVal){
				// ngModelCtrl.$setValidity()
			// })
			ngModelCtrl.$validators['notContain'] = function(viewVal, modelVal){
				return !(viewVal || '').match('batman');
			}
			//ngModelCtrl.$asyncValidators
			//ngModelCtrl.$pending
		}
	}
})
.directive('asyncNameExists', function(Users) {

	return {
		restrict:'A',
		require: 'ngModel',
		link: function(scope, element, attrs, ngModelCtrl){

			// scope.$watch(attrs['notContain'], function(newVal){
				// ngModelCtrl.$setValidity()
			// })
			ngModelCtrl.$asyncValidators['nameExists'] = function(viewVal, modelVal){
				return Users.checkNameExists(viewVal)
				
			}
			//ngModelCtrl.$asyncValidators
			//ngModelCtrl.$pending
		}
	}
})