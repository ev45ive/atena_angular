angular.module('tasks')
.controller('TasksCtrl', TasksCtrl);

// ....

function TasksCtrl($scope, Tasks){

	console.log('TasksCtrl')

	Tasks.getTasks().then(function(tasksQuerySet){
		$scope.tasks = tasksQuerySet;
	})

	$scope.addTask = function(name){

		Tasks.addTask({
			title: name,
			completed: false
		})
		$scope.name = ''
	}

	// Tasks.addTask({
	// 	name:'Nauczyć się AngularJS!', 
	// 	completed: false
	// })
}