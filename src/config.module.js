angular.module('config', [])

.factory('myInterceptor', function(Session) {
        return {
            'request': function(config) {
                    config.headers['Authorization'] = 'Bearer lubieplackimalinowe'
                    console.log(config)
                    return config;
                }
                /*
                	'requestError'
                	'response'
                	'responseError'
                */
        }
    })
    .service('Session', function() {

    })

// Config / Provide Phase
.config(function(ServerApiProvider, $httpProvider) {

    console.log('defaults', $httpProvider.defaults)

    $httpProvider.interceptors.push('myInterceptor')

    ServerApiProvider.setServerUrl('http://localhost:3000/')
    ServerApiProvider.addServerResource('users', {})
    ServerApiProvider.addServerResource('todos', {})

})

.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider
        .otherwise('/users')

    $stateProvider
        .state('root', {
        	url:'/',
        	templateUrl: 'layout.tpl.html'
    	})
        .state('root.tasks', {
            url: 'tasks',
            templateUrl: 'tasks/tasks.tpl.html'
        })
        .state('root.users', {
            url: 'users',
            templateUrl: 'users/users.tpl.html',
        })
        .state('root.users.show', {
            url: '/:user_id',
            resolve:{
            	actions: function($state, Users){
            		return {
            			go: function(sref){
            				$state.go(sref)
            			},
            			saveUser: function(user){
            				return Users.saveUser(user)
            			}
            		}
            	},
            	selected: function(Users, $stateParams){
            		var id = parseInt($stateParams['user_id']);
            		if(id){
            			return Users.getUser(id)
            		}else{
            			return {}
            		}
            	}
            },
            template: '<ui-view></ui-view>'
        })
	        .state('root.users.show.details', {
	            url: '/details',
	            template: `
				<user-detail user="$resolve.selected">
					<button ui-sref="^">Cancel</button>

					<button ui-sref="^.form({user_id:$resolve.selected.id})">
						Edit</button>
				</user-detail>
	            `
	        })
	        .state('root.users.show.form', {
	            url: '/form',
	            controller: function(){
	            	console.log('ctrl')
	            },
	            template: `
	    		<user-form
					user="$resolve.selected" 
					on-save="$resolve.actions.saveUser(user)"
					on-cancel="$resolve.actions.go('^') ">
				</user-form>
				`
	        })
        .state('root.scopes', {
            url: 'scopes',
            templateUrl: 'scopes/scopes.tpl.html'
        })

})



//================================================
// Run Phase
.run(function(Users) {
    console.log(Users)

})
