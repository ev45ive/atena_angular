
angular.module('users')
.provider('ServerApi', function(){

	var server_url, resouces = {};

	return { // ServerApiProvider
		setServerUrl: function(url){
			server_url = url;
		},
		addServerResource: function(name){
			resouces[name] = name;
		},
		$get: function(Session, $rootScope){
			return {  // ServerApi
				getResource(name){
					return server_url + resouces[name] + '/';
				},
				login: function(){
					// $http.post(...login)
					//Session.setToken()
					// $rootScope.$broadcast('logged-in')
					$rootScope.loggedIn = true;
				},
				logout: function(){
					// $http.post(...logout)
					//Session.clearToken()
					// $rootScope.$broadcast('logged-out')
					$rootScope.loggedIn = false;
				}
			}
		}
	}
})

.service('Users', function($http, ServerApi){

	ServerApi.logout();

	this.querySet = {
		items: []
	}
	this.server_url = ServerApi.getResource('users');

	this.checkNameExists = function(name){
		return $http.get(this.server_url+'?name='+name)
		.then(function(response){
			 return response.data.length == 0 ? true : Promise.reject('error')
		})
	}

	this.saveUser = function(user){
		console.log(user)
		if(user.id){
			var request = $http
			 	.put(this.server_url+user.id,user);
		}else{
			var request = $http
			 	.post(this.server_url,user);
		}

		return request
		 .then(function(response){
		 	this.getUsers()
	 	 }.bind(this))
	};

	this.getUser = function(id){
		return $http
		 .get(this.server_url+id)
		 .then(function(response){
		 	return response.data;
		 }.bind(this))
	};

	this.getUsers =  function(){
		return $http
		 .get(this.server_url+'?_page=1')
		 .then(function(response){
		 	console.log('getUsers',response.data );
		 	this.querySet.items = response.data;
		 	return this.querySet;
		 }.bind(this))
	};
})