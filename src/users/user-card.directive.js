angular.module('users')
//  userCard => <user-card>
.directive('userCardOld',function(/*$templateCache, Users */) {

	// directive-definition-object - DDO
	return {
		//restrict: 'EACM', //'EA'
		//template: '<div>Hello!</div>'
		//priority:0,
		// template: function(el, attrs){
		// 	return '<div>'
		// },
		transclude:{
			header:'?header'
		},
		templateUrl: 'users/user.tpl.html',
		scope:{
			title: '@', // as String
			user: '=', // '=user' // interpolate // 2-way
			onSaved: '&onSave'
		},
		bindToController:true,
		controller: function(){
			this.save = function(){
				this.onSaved({user: this.user})
			}
		},
		controllerAs: '$ctrl',
		// link: function(scope, el, attrs, transclude ){
		// 	var expr = attrs['user'];

		//var template = transclude(scope)

		// 	scope.$watch(expr,function(newVal,oldVal){
		// 		console.log('$watch:user',newVal,oldVal)
		// 		scope.user = newVal;
		// 	});
		// }

	}

})