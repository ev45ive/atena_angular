angular.module('myApp')
    .component('tabs',  {
        template: `
			<ul class="nav nav-tabs">
			  <li ng-repeat="tab in $ctrl.tabs" ng-class="{
			  	active: tab == $ctrl.activeTab
			  }" ng-click="$ctrl.select(tab)">
			  	<a href="#">{{tab.title}}</a>
		  	  </li>
			</ul>
			<div ng-transclude></div>
		`,
        transclude: true,
        controller: function(){

        	this.tabs = []
        	this.activeTab = null;
        	this.addTab = function(tab,defaultTab){
        		this.tabs.push(tab)
        		if(!this.activeTab || defaultTab){
        			this.select(tab)
        		}
        	}
        	this.select = function(selected){
        		this.activeTab = selected;
        		this.tabs.forEach(function(tab){
        			tab.show = this.activeTab == tab;
        		}.bind(this))
        	}
        }
    })
    .component('tab',  {
        template: '<div class="tab" ng-if="$ctrl.show" ng-transclude></div>',
        transclude: true,
        require:{
        	tabs: '^tabs'
        },
        bindings:{
        	title:'@',
        	default:'=?'
        },
        controller: function(){
        	this.show = false;

        	this.$onInit = function(){
        		this.tabs.addTab(this, this.default)
        		console.log(this.tabs)
        	}
        	this.$onDestroy = function(){
        		console.log('Destroy!')
        		// this.tabs.removeTab(this)
        	}
        }
    })
