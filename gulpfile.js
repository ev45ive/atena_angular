// npm install --global gulp-cli
// npm install --save-dev gulp
// gulp --tasks

var gulp = require('gulp');
var debug = require('gulp-debug');
var browserSync = require('browser-sync');
var server = browserSync.create();
var rename = require('gulp-rename');
var inject = require('gulp-inject');
var watch = require('gulp-watch');
var wiredep = require('gulp-wiredep');
var useref = require('gulp-useref');
var gulpIf = require('gulp-if');
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate');
var ngTemplates = require('gulp-ng-templates');
var addsrc = require('gulp-add-src');
var es = require('event-stream');
// plugins = gulpLoadPlugins()

var del = require('del');

gulp.task('clean', function(){
	return del([
		'./dist/**/*'
	])
})

gulp.task('default',['copy'], function() {
	// body...
	console.log('default task hello')
});

gulp.task('templates',function(){
	return gulp.src([
		'./src/**/*.tpl.html',
		'!./src/index.tpl.html'
		])
		.pipe(ngTemplates({
			 filename: 'templates.module.js',
			  module: 'app_templates',
		}))
		.pipe(gulp.dest('./.tmp'))
})

gulp.task('build', ['inject','templates'], function(){

	return gulp.src('./src/index.html')
		.pipe(useref({ 
			searchPath: ['./.tmp','./src']
			//additionalStreams: [templatesStream]
		}))
		.pipe(debug())
		.pipe(gulpIf('*.js', ngAnnotate() ))
		.pipe(gulpIf('scripts/*.js', uglify()
			.on('error',function(e){
				console.log(e)
			} )
		))
		//.pipe(gulpIf('**/*.css', uglify() ))
		.pipe( gulp.dest('./dist'))
})

gulp.task('json-server', function(){
	var jsonServer = require('json-server')
	var server = jsonServer.create()
	var router = jsonServer.router('db-1481034459820.json')
	var middlewares = jsonServer.defaults()

	server.use(middlewares)

	// Możemy go rozszerzać o dodatkowe funkcjonalności !!!
	// To handle POST, PUT and PATCH you need to use a body-parser
	// You can use the one used by JSON Server
	server.use(jsonServer.bodyParser)
	server.use(function (req, res, next) {
	  if (req.method === 'POST') {
	    req.body.createdAt = Date.now()
	  }
	  // Continue to JSON Server router
	  next()
	})

	function isAuthorized(req){
		return req.headers['authorization'] == 'Bearer lubieplackimalinowe'
		//return true;
	}

	server.use(function (req, res, next) {
	 if (isAuthorized(req)) { // add your authorization logic here
	   next() // continue to JSON Server router
	 } else {
	   res.sendStatus(401)
	 }
	})

	server.use(router)
	server.listen(3000, function () {
	  console.log('JSON Server is running')
	})
})

gulp.task('inject', function(){
	return gulp.src('./src/index.tpl.html')
		.pipe(rename('index.html'))
		.pipe(wiredep())
		.pipe(inject( gulp.src([
			'./src/**/*.module.js',
			'./src/**/*.js',
			'!./src/bower_components/**',
			'./src/**/*.css',
		]), {relative: true } ))
		.pipe(gulp.dest('./src'))
});

gulp.task('serve-json', ['serve','json-server'], function(){

})

gulp.task('serve',['inject'], function(){
	server.init({
		server:{
			baseDir:'./src/'
		},
		port: 8080
	})
	gulp.watch('./src/*.tpl.html', ['reload-inject']);
	watch('./src/**/*.js',  function(){
		gulp.run(['reload'])
	});
	watch(['./src/**/*.js'],{events:['add','unlink']}, function(){
		gulp.run(['reload-inject'])
	});
});

gulp.task('reload-inject',['inject'],function(){
	server.reload()
})
gulp.task('reload',function(){
	server.reload()
})


gulp.task('copy', function() {
	return gulp.src(['./src/**/*.*'])
		.pipe(debug())
		.pipe( gulp.dest('./dist') )
});