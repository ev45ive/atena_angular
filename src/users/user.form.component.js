angular.module('users')
.component('userForm', {
	templateUrl: 'users/user.form.tpl.html',
	bindings:{
		title: '@',
		data: '<user',
		onSaved: '&onSave',
		onCancel: '&onCancel'
	},
	transclude:true,
	controller: UserCardCtrl
})

function UserCardCtrl($rootScope){

	$rootScope.$on('logged-out', function(){
		
	})

	this.$onChanges = function(changes){
		this.user = Object.assign({},this.data)
	}

	this.save = function(){
		this.onSaved({user: this.user})
	}
	this.cancel = function(){
		this.onCancel({user: this.user})
	}
}

