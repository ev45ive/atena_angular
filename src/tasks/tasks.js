angular.module('tasks')
.service('Tasks',function($http){

	this.userId = 108;

	this.querySet = {
		items: []
	}

	this.addTask = function(task){
		task.userId = this.userId;

		return $http
		 .post('http://localhost:3000/todos',task)
		 .then(function(response){
		 	this.getTasks()
	 	 }.bind(this))
	};

	this.getTasks =  function(){
		 	console.log('getTasks');
		return $http
		 .get('http://localhost:3000/todos?'
		 	+'userId='+this.userId
		 	+'&_page=1')
		 .then(function(response){
		 	this.querySet.items = response.data;
		 	return this.querySet;
		 }.bind(this))
	};
})